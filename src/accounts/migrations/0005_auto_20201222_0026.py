# Generated by Django 3.1 on 2020-12-22 00:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_user_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='image',
            field=models.ImageField(default='default.jpg', null=True, upload_to='pics/'),
        ),
    ]
