# Generated by Django 3.1 on 2020-12-22 00:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20201222_0013'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='image',
            field=models.ImageField(default='default.png', upload_to='pics'),
        ),
    ]
