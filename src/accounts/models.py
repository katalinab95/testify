from PIL import Image

from app import settings

from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    rating = models.SmallIntegerField(null=True, blank=True, default=0)
    image = models.ImageField(null=True, default='default.jpg', upload_to='pics/')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        if not self.image.name == 'default.jpg':
            filename = str(settings.MEDIA_ROOT + self.image.name)
            img = Image.open(filename)

            img.thumbnail((300, 300), Image.ANTIALIAS)
            img.save(filename)
