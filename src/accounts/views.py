from accounts.forms import AccountCreateForm, AccountEditPasswordForm, AccountUpdateForm
from accounts.models import User

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView


class AccountCreateView(CreateView):
    model = User
    template_name = 'registration.html'
    form_class = AccountCreateForm
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        result = super().form_valid(form)

        messages.info(self.request, 'Successful create account')

        return result


class AccountLoginView(LoginView):
    template_name = 'login.html'

    def get_redirect_url(self):
        if self.request.GET.get('next'):
            return self.request.GET.get('next')
        return reverse('core:index')

    def form_valid(self, form):
        result = super().form_valid(form)

        messages.info(self.request, f'{self.request.user} Successful authorization')

        return result


class AccountLogoutView(LoginRequiredMixin, LogoutView):
    template_name = 'logout.html'

    def get(self, request, *args, **kwargs):
        result = super().get(request, *args, **kwargs)
        user = self.request.user

        messages.info(self.request, f'User {user} Successful logged out!')

        return result


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'account.html'
    form_class = AccountUpdateForm
    success_url = reverse_lazy('core:index')

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):

        result = super().form_valid(form)

        messages.info(self.request, 'Successful change profile')

        return result


class AccountChangePasswordView(LoginRequiredMixin, PasswordChangeView):
    form_class = AccountEditPasswordForm
    template_name = 'change-password.html'
    success_url = reverse_lazy('accounts:account')

    def form_valid(self, form):
        result = super().form_valid(form)

        messages.info(self.request, 'Successful change password')

        return result


class AccountListView(ListView):
    model = User
    template_name = 'account-list.html'
    context_object_name = 'accounts'
