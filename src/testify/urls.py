from django.urls import path

from testify.views import TestListView

app_name = 'testify'

urlpatterns = [
    path('tests', TestListView.as_view(), name='tests_list'),
    ]
