from django.views.generic import ListView

from testify.models import Test


class TestListView(ListView):
    model = Test
    paginate_by = 10
    context_object_name = 'tests'
    template_name = 'tests-list.html'
